﻿using UnityEngine;
using System.Collections;

public class PlayerBehavior : MonoBehaviour {

    [SerializeField]
    private ShapeState currentStateShape;
    private GameObject currentShape;
    public GameObject armadilloNormal;
    public GameObject armadilloRollBall;
    public GameObject armadilloForce;

    enum ShapeState
    {
        normal = 0,
        ball = 1,
        force = 2
    }

	// Use this for initialization
	void Start () {

        ShapeTransform(ShapeState.normal);

    }

    void Update()
    {
        CheckInputShapeTransform();
    }


    // Check player inputs to set shape transform

    void CheckInputShapeTransform()
    {
        string inputName = Input.inputString;

        switch (inputName)
        {
            case "e":
                ShapeTransform(ShapeState.normal);
                break;

            case "r":
                ShapeTransform(ShapeState.ball);
                break;

            case "f":
                ShapeTransform(ShapeState.force);
                break;  
        }
        
    }

    // Define shape state gameObject

    void ShapeTransform(ShapeState shapeState)
    {

        GameObject newShape = null;

        currentStateShape = shapeState;

        switch (shapeState)
        {
            case ShapeState.normal:

                newShape = armadilloNormal;
                break;

            case ShapeState.ball:

                newShape = armadilloRollBall;
                break;

            case ShapeState.force:

                newShape = armadilloForce;
                break;
            
        }

        if (newShape != null)
        {

            GameObject temp;
            
            if (currentShape == null)
            {
                temp = Instantiate(newShape, transform.position, transform.rotation) as GameObject;
            }
            else
            {
                temp = Instantiate(newShape, currentShape.transform.position, currentShape.transform.rotation) as GameObject;
            }
            
            temp.transform.parent = gameObject.transform;

            DestroyImmediate(currentShape);
            currentShape = temp;

            //temp.transform.position = currentShape.transform.position;
            //temp.transform.rotation = currentShape.transform.rotation;

            //currentShape = temp;
        }

    }
}

