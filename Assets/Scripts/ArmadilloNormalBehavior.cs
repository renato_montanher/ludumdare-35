﻿using UnityEngine;
using System.Collections;

public class ArmadilloNormalBehavior : MonoBehaviour {

    [SerializeField]
    private float velocity;

    [SerializeField]
    private float jumpForce;

    [SerializeField]
    private float maxVelocity;

    private Rigidbody2D rb;
    private Animator animator;
    private bool isGrounded;

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();

        velocity = 800.0f;
        jumpForce = 350.0f;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Moviment();
	}

    void Moviment()
    {

        Vector2 inputMov;

        inputMov.x = Input.GetAxis("Horizontal") * velocity * Time.deltaTime;

        // Se jogador apertou o botão de pulo e tambem está com os pé no chao ....
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            inputMov.y = jumpForce;
        }
        else
        {
            inputMov.y = 0;
        }

        rb.AddForce(inputMov);

        if (inputMov.x > 0)
        {
            transform.eulerAngles = new Vector2(0, 0);
        }
        else
        {
            transform.eulerAngles = new Vector2(0, 180);
        }

        //Debug.Log("Normal: " + inputMov + " - Normalized: " + inputMov.normalized);
        //Debug.Log(inputMov.normalized.x * 180);


        //Processa animacoes
        animator.SetFloat("speed", inputMov.x);
        animator.SetBool("isGrounded", isGrounded);

    }

    public void SetStateGround(bool state)
    {
        isGrounded = state;
    }

}
