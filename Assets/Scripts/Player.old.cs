﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float velocidade;
    public float forcaPulo;
    public float forcaTorque;
    Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        movimentacao();
	}

    void movimentacao() {
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            transform.Translate(Vector2.right * velocidade * Time.deltaTime);
            transform.eulerAngles = new Vector2(0, 0);
            rb.AddTorque(-forcaTorque, ForceMode2D.Force);
        }

        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            transform.Translate(Vector2.right * velocidade * Time.deltaTime);
            transform.eulerAngles = new Vector2(0, 180);
            rb.AddTorque(forcaTorque,ForceMode2D.Impulse);
        }

        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(transform.up * forcaPulo);
        }
    }
}
