﻿using UnityEngine;
using System.Collections;

public class GroundCheckBehavior : MonoBehaviour {

    public GameObject shape;

	// Use this for initialization
	void Start () {

	}
	
    void OnTriggerEnter2D(Collider2D col)
    {
        shape.SendMessage("SetStateGround", true);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        shape.SendMessage("SetStateGround", false);
    }
}
